"use strict";

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}]; // tri par ordre alphabetique

/*data.sort(function (a, b) {
    if(a.name > b.name) return 1;
    if(a.name < b.name) return -1;
    else  return 0;
  });
*/
// tri par prix petit format croissant

/*
data.sort(function(a,b){
    return a.price_small - b.price_small;
});
*/
// tri par prix petit format croissant et en cas d'égalité, par prix grand format croissant

/*
data.sort(function (a, b) {
    if(a.price_small > b.price_small) return 1;
    if(a.price_small < b.price_small) return -1;
    if(a.price_small == b.price_small)  return a.price_large > b.price_large;
  });
*/

var html = "";
var dataFilter = data; //dataFilter = data.filter(element => element.base == "tomate");
//dataFilter = data.filter(element => element.price_small < 6);
//dataFilter = data.filter(element => element.name.split("i").length >2);

dataFilter.forEach(function (_ref) {
  var image = _ref.image,
      name = _ref.name,
      price_small = _ref.price_small,
      price_large = _ref.price_large;
  return html = html + "<article class=\"pizzaThumbnail\">\n        <a href=\"".concat(image, "\">\n            <img src=\"").concat(image, "\"/>\n            <section> \n                <h4>").concat(name, "</h4>\n                <ul>\n                    <li> Prix petit format : ").concat(price_small.toFixed(2), " \u20AC</li>\n                    <li> Prix grand format : ").concat(price_large, " \u20AC</li>\n                </ul>\n            </section>\n        </a>\n     </article>");
});
data.sort(function (a, b) {
  return a.name - b.name;
});
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map